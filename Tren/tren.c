﻿#include "tren.h"

PolarExpress * createTrain(int _wagonNumb)
{
	PolarExpress* train=(PolarExpress*)malloc(sizeof(PolarExpress));
	train->startingNumbOfWagon = _wagonNumb;
	if (_wagonNumb<1){
		train->last = NULL;
		train->first = NULL;
		return train;
	}
	Wagon* wagon = (Wagon*)malloc(sizeof(Wagon));
	*wagon = createWagon();
	wagon->numb = 1;
	wagon->next = NULL;
	wagon->previous = NULL;

	train->first = wagon;
	train->last = wagon;

	
	for (int i=2; i<=_wagonNumb; i++)
	{
		Wagon* wagon = (Wagon*)malloc(sizeof(Wagon));
		*wagon = createWagon();
		wagon->numb = i;
		wagon->next = NULL;
		wagon->previous = train->last;
		train->last->next = wagon;
		train->last = wagon;
	}

	

	return train;
}

Ticket* buyNewTicket(PolarExpress* _train, Ticket* _ticket)
{
	if (_train==NULL || _train->first==NULL)
	{
		return NULL;
	}
	
	Wagon* pNode = _train->first;

	if (pNode->next==NULL)
	{
		for (int i = 0; i < places_length; i++)
		{
			if (pNode->p[i] == false)
			{
				_ticket->place = i;
				_ticket->wagonNum = pNode->numb;
				pNode->p[i] = true;
				return _ticket;
			}
			
		}
			return NULL;
	}

	while(pNode->next!=NULL){

		for (int i=0; i<places_length; i++)
		{
			if (pNode->p[i]==false)
			{
				_ticket->place = i;
				_ticket->wagonNum = pNode->numb;
				pNode->p[i] = true;
				return _ticket;
			}
		}
			pNode = pNode->next;
	}


	//Возвращается ли поинтер, который был сделан в функции, если его не создать маллоком?

	return NULL;
}

Wagon createWagon()
{


	Wagon newWagon;
	for (int i = 0; i < places_length; i++) {
		newWagon.p[i] = false;
	}


	return newWagon ;
}

PolarExpress * checkTrain(PolarExpress * _train)
{
	if (_train==NULL || _train->first==NULL)
	{
		return NULL;
	}
	
	Wagon* pNode = _train->first;
	Wagon* ppNode;
	bool mark = false;

	
	if (pNode->next==NULL)//nur ein element in der liste
	{


		mark = iteration(pNode);

		if (!mark)
		{
			free(pNode);
			_train->first = NULL;
			_train->last = NULL;
			return _train;
		}
		
	}
	

	while (pNode->next!=NULL)
	{


		mark = iteration(pNode);
		
		
		if (!mark && pNode == _train->first)//popFront
		{
			ppNode = pNode->next;
			ppNode->previous = NULL;
			_train->first= ppNode;
			free(pNode);
			pNode = ppNode;

			mark = iteration(pNode);

		}


		if (!mark && pNode==_train->last)//popLast
		{
			_train->last = pNode->previous;
			_train->last->next = NULL;
			free(pNode);
			return _train;
		}
		else if (!mark)
		{
			ppNode = pNode->next;
			pNode->previous->next = pNode->next;
			pNode->next->previous = pNode->previous;
			free(pNode);
			pNode = ppNode;

			if (pNode==_train->last)
			{
				mark = iteration(pNode);
				
				if (!mark)
				{
					_train->last = pNode->previous;
					_train->last->next = NULL;
					free(pNode);
					return _train;
				}
			}

		}
		pNode = pNode->next;
		mark = false;
		
	}

	return NULL;
}

bool iteration(Wagon * _wagon)
{
	bool marke=false;

	for (int i = 0; i < places_length; i++)
	{
		if (_wagon->p[i] == true)
		{
			_wagon = true;
			return true;
		}
	}

	return false;
}



bool push(PolarExpress * _train, Wagon * _wagon)
{//Alle pointer übernimmt der Zug

	if (_train==NULL)
	{
		return false;
	}

	_train->startingNumbOfWagon++;
	_wagon->numb = _train->startingNumbOfWagon;
	_wagon->next = NULL;
	
	if (_train->first==NULL)
	{
		
		_wagon->previous = NULL;
		_train->first = _wagon;
		
		
	}
	else{
		_wagon->previous = _train->last;
		_train->last->next = _wagon;
	}

		_train->last = _wagon;

	return true;
}

PolarExpress * separate( PolarExpress * _train1, unsigned int _numb)
{
	if (_train1==NULL || _train1->first==NULL || _train1->first->next==NULL || _numb>_train1->startingNumbOfWagon)
	{
		return NULL;
	}

	PolarExpress * train2=(PolarExpress*)malloc(sizeof(PolarExpress));
	Wagon* pNode = _train1->first;

	
	unsigned short num2=1;
	

	while (num2<=_numb)
	{

		if (num2==_numb)
		{
			if (pNode->next==NULL)
			{
				train2->first = NULL;
				train2->last = NULL;
				return train2;
			}
			train2->first = pNode->next;
			train2->first->previous = NULL;
			train2->last = _train1->last;

			_train1->last = pNode;
			_train1->last->next = NULL;
		}

		num2++;
		pNode = pNode->next;

	}

	return train2;
}
