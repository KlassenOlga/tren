#pragma once
#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#define places_length 20

typedef struct Ticket{
	unsigned int place;
	unsigned int wagonNum;

}Ticket;


typedef struct Wagon{
	unsigned int numb;
	bool p[places_length];
	struct Wagon* next;
	struct Wagon* previous;
}Wagon;

typedef struct PolarExpress {
	Wagon* first;
	Wagon*last;
	unsigned int startingNumbOfWagon;

}PolarExpress;

PolarExpress* createTrain(int _wagonNumb);
bool push(PolarExpress* _train, Wagon* _wagon);
PolarExpress* separate(PolarExpress* _train1 ,unsigned int _numb);
Ticket* buyNewTicket(PolarExpress* _train, Ticket* _ticket);
Wagon createWagon();
PolarExpress* checkTrain(PolarExpress* _train);
bool iteration(Wagon* _wagon);